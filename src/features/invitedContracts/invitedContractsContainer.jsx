import React from 'react'
import PropTypes from 'prop-types'

import { connect } from 'react-redux'

const handleContractsClick = roleId => console.log(roleId)

const InvitedContractsContainer = props => {
  const { roleId } = props

  return (
    <button onClick={() => handleContractsClick(roleId)}>
      Invited Candidates
    </button>
  )
}

InvitedContractsContainer.propTypes = {
  roleId: PropTypes.number.isRequired,
  invitedContractsList: PropTypes.arrayOf(PropTypes.shape({
    candidateName: PropTypes.string.isRequired,
  })),
}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(InvitedContractsContainer)
